# {{ .Title }}
{{ if .Content }}
{{ .Content }}{{ end }}
{{ range .Pages }}=> {{ .Path }} {{ if not .Date.IsZero -}}
{{ .Date.Format "2006-01-02" }} - {{end}}{{ .Title }}
{{ end -}}

---

=> /contact 💬 Une question, une remarque, contactez-moi :)

=> / 🏠 retour à la page principale
