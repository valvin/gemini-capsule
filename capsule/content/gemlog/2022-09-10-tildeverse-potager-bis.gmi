---
title: Tildeverse et potager
date: 2022-09-11
params:
  tags: tildeverse,potager
---

### Tildeverse

Ce matin j'ai reçu l'acceptation de ma demande d'adhésion au tildverse cheztilde.club. Je me suis donc lancé dans l'aventure et découvre de nouvelleschoses. Avec l'accès au serveur, je bénéficie également d'une capsule geminidans laquelle j'écrirais un peu ce que je découvre la-bas, en anglais cettefois-ci.

=> gemini://tilde.club/~valvin

### Potager

J'ai mis en sachet mes graines de basilic et coriandre restantes. J'ai récoltémes graines d'aneth et commencer celle de radis. Pour l'instant je n'ai  pasgrand chose en radis mais il faut que j'attende le reste, ce n'est pas encoreassez sec. En faisant une recherche sur le sujet, j'ai découvert le site"diyseed" qui propose du contenu texte et vidéo sur les semences:

=> https://www.diyseeds.org/fr


