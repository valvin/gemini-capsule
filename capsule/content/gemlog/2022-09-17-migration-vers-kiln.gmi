---
title: Migration vers kiln
date: 2022-09-17
params:
  tags: gemini,kiln,agate
---

## Kiln

En échangeant sur Station, on m'a fait découvrir la capsule unbon.cafe quipromeut l'usage de kiln pour la génération des différentes capsules hébergées.En effet, l'outils m'a rapidement séduit, d'autant plus qu'il est très proche dece que je connais de l'outils équivalent hugo.

Du coup, c'est parti! Je migre le maigre contenu de cette capsule avec ce nouveloutils qui va me permettre d'avoir plus de libertés et même un flux :)

Je pense avoir compris globalement le fonctionnement mais il me reste encorequelques inconnus à éclaircir. J'ai reporté tout le contenu en sépérant lesdifférents jours pour que ce soit plus cohérent. Le flux prend en compteuniquement les billets du gemlog.

J'ai du faire quelques modifications de mon image agate pour prendre en comptece changement. Afin d'éviter de compiler kiln, j'ai du passer de debian à alpinecomme base d'image. Mais de ce fait, j'ai du compiler agate et je ne sais paslequel des deux était moins lourd :)

Après quelques péripéties car je n'avais pas fait les tests complets, je penseque l'image est opérationnelle et cette capsule également. Pour les curieux, jemets le lien de deux dépôts git:

=> https://gitlab.com/valvin/gemini-agate-image Image Docker gemini-agate-image
=> https://framagit.org/valvin/gemini-capsule Le contenu de cette capsule
