---
title: Gitlab CE installation sur debian
date: 2022-09-15
params:
  tags: gitlab
---

### Gitlab CE sur debian

J'ai testé rapidement l'installation d'un gitlab communautaire dans une VM surmon poste de travail (Fedora). Je pensais qu'en utilisant l'utilitaire*Machines* pour une VM ce serait simple, mais non. Par défaut, je ne pouvais pascommuniquer depuis ma machine vers la VM car la partie réseau n'était pasconfigurée. De ce fait, je me suis appuyé sur une documentation Fedora [1] qui m'afait découvrir `virt-manager` avec `libvirt`. J'ai quand même pu récupérerl'image que j'avais initialisé dans *Machines*. 

Une fois la connexion ssh activée sur ma VM, j'ai déroulé l'installation deGitlab en suivant la procédure [2]. Je n'ai pas tenté de configurerl'installation et j'ai uniquement paramétré l'url de mon instance. Par défaut,si on met https://, un certificat letsencrypt est demandé. Ce qui dans mon casn'était pas une très bonne idée. J'ai donc continué sans certificat... bouh!!

L'installation est globalement rapide et installe beaucoup de composants.Certainement plus que ce dont j'en ai besoin. J'ai récupéré le mot de passe rootcomme indiqué et j'ai pu parcourir mon instance toute neuve. J'ai parcourru lesdifférentes configurations et il y a en quelques unes! Je pense que pour bienfaire, il faut lister les fonctionnalités dont on a besoin et voir pour chacunesd'elles le paramétrage nécessaire.

Le point positif de ce type de recherche c'est que l'on découvre desfonctionnalités dont on ne connaissait pas l'existance. Par exemple plantUML quipermet de faire des digrammes de séquences, activé par défaut sur gitlab.commais qui nécessite une installation supplémentaire pour une instance perso.

=> https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-virtualization/ [1]
=> https://about.gitlab.com/install/#debian [2]

