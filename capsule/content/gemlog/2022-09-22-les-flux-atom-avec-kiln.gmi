---
title: les flux atom avec kiln
date: 2022-09-22
params:
  tags: gemini,rss,kiln

---

Pour naviguer sur gemini, j'utilise depuis peu amfora. Initialement, j'utilisaisLagrange. Je n'ai pas encore réalisé beaucoup de souscriptions à des flux maisc'est une fonctionnalité qui m'interesse. Et donc, naturellement je souhaitaisque le flux de ce gemlog fonctinone. Et ... suspens, ce n'était pas le cas.

Il s'avère que kiln propose un template pour générer un flux atom mais celui-cine contient pas un élément à priori obligatoire, la balise "link" avec"rel=alternate" pour un élément "entry":

```
<link rel="alternate" href="gemini://xxx" /> 
```

Du coup, en apportant la modification au template "atom.xml" fournit par défaut c'est maintenant opérationnel:

```
{{ `<?xml version="1.0" encoding="utf-8"?>` | safeHTML }}
<feed xmlns="http://www.w3.org/2005/Atom">
<id>{{ .URL }}</id>
<title>{{ .Title }}</title>
<updated>{{ site.Generated.Format "2006-01-02T15:04:05Z07:00" }}</updated>
<link href="{{ .URL | safeURL }}" rel="alternate"/>
{{ range .Pages }}<entry>
        <link href="{{ .URL | safeURL }}" rel="alternate"/>
	<id>{{ .URL }}</id>
	<title>{{ .Title }}</title>
	<updated>{{ .Date.Format "2006-01-02T15:04:05Z07:00" }}</updated>
</entry>
{{ end -}}
</feed>
```

=> /atom.xml Le flux de ce gemlog corrigé

Mais comme je pensais que c'était les flux atom qui posait problème, je me suis renseigné sur les flux décrit par le protocole Gemini. Ils en parlent ici:

=> gemini://gemini.circumlunar.space/docs/companion/subscription.gmi

J'ai donc tenté de l'implémenter également avec:

```
# {{ .Title }} 

This a generated page to allow feed subscription.

{{ range .Pages }}=> {{ .URL }} {{ .Date.Format "2006-01-02" }} - {{ .Title }}
{{ end -}}
```

Cela ne fonctionne pas correctement dans "amfora" ou alors je n'ai pas su le faire fonctionner ;-). Cependant c'est opérationnel dans "Lagrange".

=> /atom.gmi le flux de ce gemlog façon gemini

Donc maintenant on peut souscrire à mon flux par deux méthodes différentes :)
