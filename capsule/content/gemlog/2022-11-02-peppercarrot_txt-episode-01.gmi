---
title: Pepper&Carrot.txt - épisode 01
date: 2022-11-02
params:
  tags: peppercarrot.txt
---

J'en avais parlé il y a quelque temps, je souhaite retranscrire les épisodes de Pepper&Carrot de David Revoy sous un format bien différent de l'original: le format texte puis j'espère dans un second temps au format audio. En plus de me faire découvrir de nouvelles choses, cela permettra peut-être à d'autres personnes de découvrir le joli projet de David Revoy.

Après une première version, Nicolas, contributeur de Pepper&Carrot et auteur de Pepper&Carrot Mini, m'a donné quelques conseils pour améliorer le texte. J'ai donc tenté avec cette seconde version, de mieux présenter les choses et apporter un peu plus de détails. Bon, on ne va pas se mentir, tout comme pour le dessin ou les échecs je suis un débutant sur ce sujet, donc ne vous attendez pas à de la grande littérature :)

Même si en tant que contributeur et fan de Pepper&Carrot, je connais assez bien la bande-dessinée, le fait de faire cette version texte m'a permis de découvrir de nouvelles choses, des détails que je n'avais pas vus sur cet épisode 01 qui est pourtant très court. J'ai hâte de voir tout ce que je découvrirais dans le prochain épisode.

Je suis curieux d'avoir vos retours, bons ou mauvais n'hésitez pas :)

Bonne lecture:

=> /peppercarrot_txt/01_potion-d-envol Pepper&Carrot.txt - épisode 01 - La potion d'envol
